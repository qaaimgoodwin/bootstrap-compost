<section id="cta">
    <div class="expanded row">
        <div class="medium-8 medium-offset-2 columns">
            <h2 class="text-center">Stay Connected</h2>
            <p class="text-center">
                SIGN UP FOR OUR NEWSLETTER AND STAY INFORMED ABOUT EVENTS, HOW-TOS, OFFERS, AND MORE.
            </p>
            <form>
                <div class="row" style="padding:5px;">
                    <div class="medium-8 medium-offset-2 columns">
                        <label>
                            <input type="text" placeholder="Enter your email address">
                        </label>
                        <div class="text-center">
                            <label class="button">Submit</label>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>