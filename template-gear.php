<?php
/*
Template Name: Gear
*/
?>

<?php get_header(); ?>
	<section>
		<div class="expanded row">
			<div class="medium-12 columns">
				<div class="featured-image-header">
					<h1 class="text-center hero-h1-bigger"><?php the_title(); ?></h1>
					<?php if ( has_post_thumbnail()) : the_post_thumbnail( 'full' ); endif; ?>
				</div>
			</div>
		</div>
	</section>
	<section class="page-content">
		<div class="wrap">
			<div id="content" class="content_padding">
			
				<div id="inner-content" class="row">
			
				    <main id="main" class="medium-8 medium-offset-2 columns" role="main">
						
						<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

							<?php get_template_part( 'parts/loop', 'page' ); ?>
							
						<?php endwhile; endif; ?>							

					</main> <!-- end #main -->
				    
				</div> <!-- end #inner-content -->
			
			</div> <!-- end #content -->
		</div>
	</section>
	<section class="gear">
	    <div class="row">
	        <div class="small-full columns">
	            <h2 class="gear-h2">GEAR</h2>
	        </div>
	    </div>
	    <div class="wrap">
	        <div class="gearSlider">
	          <div>
	          <img class="float-center" src="<?php echo get_template_directory_uri(); ?>/assets/images/people/people-blank.jpg" alt="">
	          <div class="row">
	              <div class="small-full columns">
	                  <h3 class="gear-h3">Lorem ipsum dolor sit amet.</h3>
	                  <p class="gear-p">Lorem ipsum dolor sit amet.</p>
	              </div>
	          </div>
	          </div>
	          <div class="">
	          <img class="float-center" src="<?php echo get_template_directory_uri(); ?>/assets/images/people/people-blank.jpg" alt="">
	          <div class="row">
	              <div class="small-full columns">
	                  <h3 class="gear-h3">Lorem ipsum dolor sit amet.</h3>
	                  <p class="gear-p">Lorem ipsum dolor sit amet.</p>
	              </div>
	          </div>
	          </div>
	          <div class="">
	          <img class="float-center" src="<?php echo get_template_directory_uri(); ?>/assets/images/people/people-blank.jpg" alt="">
	          <div class="row">
	              <div class="small-full columns">
	                  <h3 class="gear-h3">Lorem ipsum dolor sit amet.</h3>
	                  <p class="gear-p">Lorem ipsum dolor sit amet.</p>
	              </div>
	          </div>
	          </div>
	          <div class="">
	          <img class="float-center" src="<?php echo get_template_directory_uri(); ?>/assets/images/people/people-blank.jpg" alt="">
	          <div class="row">
	              <div class="small-full columns">
	                  <h3 class="gear-h3">Lorem ipsum dolor sit amet.</h3>
	                  <p class="gear-p">Lorem ipsum dolor sit amet.</p>
	              </div>
	          </div>
	          </div>
	          <div class="">
	          <img class="float-center" src="<?php echo get_template_directory_uri(); ?>/assets/images/people/people-blank.jpg" alt="">
	          <div class="row">
	              <div class="small-full columns">
	                  <h3 class="gear-h3">Lorem ipsum dolor sit amet.</h3>
	                  <p class="gear-p">Lorem ipsum dolor sit amet.</p>
	              </div>
	          </div>
	          </div>
	          <div class="">
	          <img class="float-center" src="<?php echo get_template_directory_uri(); ?>/assets/images/people/people-blank.jpg" alt="">
	          <div class="row">
	              <div class="small-full columns">
	                  <h3 class="gear-h3">Lorem ipsum dolor sit amet.</h3>
	                  <p class="gear-p">Lorem ipsum dolor sit amet.</p>
	              </div>
	          </div>
	          </div>
	          <div class="">
	          <img class="float-center" src="<?php echo get_template_directory_uri(); ?>/assets/images/people/people-blank.jpg" alt="">
	          <div class="row">
	              <div class="small-full columns">
	                  <h3 class="gear-h3">Lorem ipsum dolor sit amet.</h3>
	                  <p class="gear-p">Lorem ipsum dolor sit amet.</p>
	              </div>
	          </div>
	          </div>
	          <div class="">
	          <img class="float-center" src="<?php echo get_template_directory_uri(); ?>/assets/images/people/people-blank.jpg" alt="">
	          <div class="row">
	              <div class="small-full columns">
	                  <h3 class="gear-h3">Lorem ipsum dolor sit amet.</h3>
	                  <p class="gear-p">Lorem ipsum dolor sit amet.</p>
	              </div>
	          </div>
	          </div>
	          <div class="">
	          <img class="float-center" src="<?php echo get_template_directory_uri(); ?>/assets/images/people/people-blank.jpg" alt="">
	          <div class="row">
	              <div class="small-full columns">
	                  <h3 class="gear-h3">Lorem ipsum dolor sit amet.</h3>
	                  <p class="gear-p">Lorem ipsum dolor sit amet.</p>
	              </div>
	          </div>
	          </div>
	          <div class="">
	          <img class="float-center" src="<?php echo get_template_directory_uri(); ?>/assets/images/people/people-blank.jpg" alt="">
	          <div class="row">
	              <div class="small-full columns">
	                  <h3 class="gear-h3">Lorem ipsum dolor sit amet.</h3>
	                  <p class="gear-p">Lorem ipsum dolor sit amet.</p>
	              </div>
	          </div>
	          </div>
	          <div class="">
	          <img class="float-center" src="<?php echo get_template_directory_uri(); ?>/assets/images/people/people-blank.jpg" alt="">
	          <div class="row">
	              <div class="small-full columns">
	                  <h3 class="gear-h3">Lorem ipsum dolor sit amet.</h3>
	                  <p class="gear-p">Lorem ipsum dolor sit amet.</p>
	              </div>
	          </div>
	          </div>
	          <div class="">
	          <img class="float-center" src="<?php echo get_template_directory_uri(); ?>/assets/images/people/people-blank.jpg" alt="">
	          <div class="row">
	              <div class="small-full columns">
	                  <h3 class="gear-h3">Lorem ipsum dolor sit amet.</h3>
	                  <p class="gear-p">Lorem ipsum dolor sit amet.</p>
	              </div>
	          </div>
	          </div>
	          <div class="">
	          <img class="float-center" src="<?php echo get_template_directory_uri(); ?>/assets/images/people/people-blank.jpg" alt="">
	          <div class="row">
	              <div class="small-full columns">
	                  <h3 class="gear-p">Lorem ipsum dolor sit amet.</h3>
	                  <p class="gear-p">Lorem ipsum dolor sit amet.</p>
	              </div>
	          </div>
	          </div>
	        </div>
	        <div class="wrap">
	        	<p>Our homemade patches and prints are washer and dryer safe. Bootstrap is not responsible for lost or stolen property.</p>
	        </div>
	    </div>

	</section>
	<section class="email-cap">
	<div class="row wrap">
	<div class="medium-4 large-4 columns">
	    <h2 class="email-h2">Stay Connected</h2>
	</div>
	    <div class="medium-8 large-8 columns">
	        <div class="row">
	            <div class="medium-full large-full columns">
	                <h3 class="email-h3">Sign up for our newsletter and stay informed about events, how-tos, offers, and more.</h3>
	            </div>
	            <div class="medium-full large-full columns">
	                <div class="input-group">
	                  <input placeholder="Enter your email address" class="input-group-field" type="text">
	                  <div class="input-group-button">
	                    <input type="submit" class="button" value="Submit">
	                  </div>
	                </div>            
	            </div>
	        </div>
	    </div>
	</div>    
	</section>
<?php get_footer(); ?>