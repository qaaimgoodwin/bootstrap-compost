<?php
/*
Template Name: Full Width (No Sidebar)
*/
?>

<?php get_header(); ?>
	<section>
		<div class="expanded row">
			<div class="medium-12 columns">
				<div class="featured-image-header">
					<h1 class="text-center hero-h1-bigger"><?php the_title(); ?></h1>
					<?php if ( has_post_thumbnail()) : the_post_thumbnail( 'full' ); endif; ?>
				</div>
			</div>
		</div>
	</section>
	<section class="page-content">
		<div class="wrap">
			<div id="content" class="content_padding">
			
				<div id="inner-content" class="row">
			
				    <main id="main" class="medium-8 medium-offset-2 columns" role="main">
						
						<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

							<?php get_template_part( 'parts/loop', 'page' ); ?>
							
						<?php endwhile; endif; ?>							

					</main> <!-- end #main -->
				    
				</div> <!-- end #inner-content -->
			
			</div> <!-- end #content -->
		</div>
	</section>

<?php get_footer(); ?>