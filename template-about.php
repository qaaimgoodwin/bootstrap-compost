<?php
/*
Template Name: About
*/
?>

<?php get_header(); ?>
	<section>
		<div class="expanded row">
			<div class="medium-12 columns">
				<div class="featured-image-header">
					<h1 class="text-center hero-h1-bigger"><?php the_title(); ?></h1>
					<?php if ( has_post_thumbnail()) : the_post_thumbnail( 'full' ); endif; ?>
				</div>
			</div>
		</div>
	</section>
	<section class="page-content">
		<div class="wrap">
			<div id="content" class="content_padding">
			
				<div id="inner-content" class="row">
			
				    <main id="main" class="medium-8 medium-offset-2 columns" role="main">
						
						<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

							<?php get_template_part( 'parts/loop', 'page' ); ?>
							
						<?php endwhile; endif; ?>							

					</main> <!-- end #main -->
				    
				</div> <!-- end #inner-content -->
			
			</div> <!-- end #content -->
		</div>
	</section>
	<section class="meet-the-team">
	    <div class="row">
	        <div class="small-full columns">
	            <h2 class="meet-the-team-h2">MEET THE TEAM</h2>
	        </div>
	    </div>
	    <div class="wrap">
	        <div class="meetTheTeamSlider">
	          <div>
	          <img class="float-center" src="<?php echo get_template_directory_uri(); ?>/assets/images/people/Andy.jpg" alt="">
	          <div class="row">
	              <div class="small-full columns">
	                  <h3 class="meet-the-team-h3">Andy Brooks</h3>
	                  <p class="meet-the-team-p">President, Founder</p>
	              </div>
	          </div>
	          </div>
	          <div class="">
	          <img class="float-center" src="<?php echo get_template_directory_uri(); ?>/assets/images/people/Igor.jpg" alt="">
	          <div class="row">
	              <div class="small-full columns">
	                  <h3 class="meet-the-team-h3">Igor Kharitonenkov</h3>
	                  <p class="meet-the-team-p">Vice Prez, Co-Founder</p>
	              </div>
	          </div>
	          </div>
	          <div class="">
	          <img class="float-center" src="<?php echo get_template_directory_uri(); ?>/assets/images/people/Faith.jpg" alt="">
	          <div class="row">
	              <div class="small-full columns">
	                  <h3 class="meet-the-team-h3">Faith Miller</h3>
	                  <p class="meet-the-team-p">Operations Manager</p>
	              </div>
	          </div>
	          </div>
	          <div class="">
	          <img class="float-center" src="<?php echo get_template_directory_uri(); ?>/assets/images/people/Emma.jpg" alt="">
	          <div class="row">
	              <div class="small-full columns">
	                  <h3 class="meet-the-team-h3">Emma Brown</h3>
	                  <p class="meet-the-team-p">Customer Service &amp; Creative Marketing</p>
	              </div>
	          </div>
	          </div>
	          <div class="">
	          <img class="float-center" src="<?php echo get_template_directory_uri(); ?>/assets/images/people/Jake.jpg" alt="">
	          <div class="row">
	              <div class="small-full columns">
	                  <h3 class="meet-the-team-h3">Jake Bison</h3>
	                  <p class="meet-the-team-p">Business Administrator</p>
	              </div>
	          </div>
	          </div>
	          <div class="">
	          <img class="float-center" src="<?php echo get_template_directory_uri(); ?>/assets/images/people/Jessie.jpg" alt="">
	          <div class="row">
	              <div class="small-full columns">
	                  <h3 class="meet-the-team-h3">Jessie Wick</h3>
	                  <p class="meet-the-team-p">Commercial Accounts Manager</p>
	              </div>
	          </div>
	          </div>
	          <div class="">
	          <img class="float-center" src="<?php echo get_template_directory_uri(); ?>/assets/images/people/Macbain.jpg" alt="">
	          <div class="row">
	              <div class="small-full columns">
	                  <h3 class="meet-the-team-h3">Andrew MacBain</h3>
	                  <p class="meet-the-team-p">Warehouse Assistant &amp; Driver</p>
	              </div>
	          </div>
	          </div>
	          <div class="">
	          <img class="float-center" src="<?php echo get_template_directory_uri(); ?>/assets/images/people/Beruk.jpg" alt="">
	          <div class="row">
	              <div class="small-full columns">
	                  <h3 class="meet-the-team-h3">Beruk Ambatchev</h3>
	                  <p class="meet-the-team-p">Warehouse Assistant</p>
	              </div>
	          </div>
	          </div>
	          <div class="">
	          <img class="float-center" src="<?php echo get_template_directory_uri(); ?>/assets/images/people/Mike.jpg" alt="">
	          <div class="row">
	              <div class="small-full columns">
	                  <h3 class="meet-the-team-h3">Mike Comando</h3>
	                  <p class="meet-the-team-p">Senior Driver</p>
	              </div>
	          </div>
	          </div>
	          <div class="">
	          <img class="float-center" src="<?php echo get_template_directory_uri(); ?>/assets/images/people/Jared.jpg" alt="">
	          <div class="row">
	              <div class="small-full columns">
	                  <h3 class="meet-the-team-h3">Jared Dunham</h3>
	                  <p class="meet-the-team-p">Driver</p>
	              </div>
	          </div>
	          </div>
	          <div class="">
	          <img class="float-center" src="<?php echo get_template_directory_uri(); ?>/assets/images/people/Shawn.jpg" alt="">
	          <div class="row">
	              <div class="small-full columns">
	                  <h3 class="meet-the-team-h3">Shawn Cain</h3>
	                  <p class="meet-the-team-p">Driver</p>
	              </div>
	          </div>
	          </div>
	          <div class="">
	          <img class="float-center" src="<?php echo get_template_directory_uri(); ?>/assets/images/people/Gen.jpg" alt="">
	          <div class="row">
	              <div class="small-full columns">
	                  <h3 class="meet-the-team-h3">Gen Cayford</h3>
	                  <p class="meet-the-team-p">Driver</p>
	              </div>
	          </div>
	          </div>
	          <div class="">
	          <img class="float-center" src="<?php echo get_template_directory_uri(); ?>/assets/images/people/Matt.jpg" alt="">
	          <div class="row">
	              <div class="small-full columns">
	                  <h3 class="meet-the-team-p">Matt van Hoff</h3>
	                  <p class="meet-the-team-p">Driver</p>
	              </div>
	          </div>
	          </div>
	        </div>
	    </div>
	</section>
	<section class="email-cap">
	<div class="row wrap">
	<div class="medium-4 large-4 columns">
	    <h2 class="email-h2">Stay Connected</h2>
	</div>
	    <div class="medium-8 large-8 columns">
	        <div class="row">
	            <div class="medium-full large-full columns">
	                <h3 class="email-h3">Sign up for our newsletter and stay informed about events, how-tos, offers, and more.</h3>
	            </div>
	            <div class="medium-full large-full columns">
	                <div class="input-group">
	                  <input placeholder="Enter your email address" class="input-group-field" type="text">
	                  <div class="input-group-button">
	                    <input type="submit" class="button" value="Submit">
	                  </div>
	                </div>            
	            </div>
	        </div>
	    </div>
	</div>    
	</section>
<?php get_footer(); ?>