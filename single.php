<?php get_header(); ?>
<section>
		<div class="expanded row">
			<div class="medium-12 columns">
				<div class="featured-image-header">
					<?php if ( has_post_thumbnail()) : the_post_thumbnail( 'full' ); endif; ?>
				</div>
			</div>
		</div>
	</section>
			
<div id="content">

	<div id="inner-content" class="row">

		<main id="main" class="medium-8 medium-offset-2 columns" role="main">
		
		    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		
		    	<?php get_template_part( 'parts/loop', 'single' ); ?>
		    	
		    <?php endwhile; else : ?>
		
		   		<?php get_template_part( 'parts/content', 'missing' ); ?>

		    <?php endif; ?>

		</main> <!-- end #main -->

		

	</div> <!-- end #inner-content -->

</div> <!-- end #content -->

<?php get_footer(); ?>