
<br/>
<div class="test_archive">
<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article">					
	
					
	<section class="entry-content_archive" itemprop="articleBody">
		<a href="<?php the_permalink() ?>"><?php the_post_thumbnail('blogpost_thumb'); ?></a>
		<header class="article-header">
		<h2 class="text-center sml_fix"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
		<?php get_template_part( 'parts/content', 'byline' ); ?>
	</header> <!-- end article header -->
		<?php 
		$content = get_the_content();
		$content = strip_tags($content);
		?>
		<p class="text-center">
		<?php 
		
		echo substr($content, 0, 250);

		?>...
		</p>
		<p class="text-center" style="padding-bottom:20px;">
		<a href="<?php the_permalink();?>" style="color:#9fc613;"><button class="read_more">READ MORE</button></a>
		</p>
	</section> <!-- end article section -->
						
	<footer class="article-footer">
    	<p class="tags"><?php the_tags('<span class="tags-title">' . __('Tags:', 'jointstheme') . '</span> ', ', ', ''); ?></p>
	</footer> <!-- end article footer -->				    						
</article> <!-- end article -->
</div><br/>
