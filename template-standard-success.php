<?php 

/*

Template Name: Standard Success Template

*/

?>
<?php

get_header();

?>
<section style="padding-top:50px;padding-bottom:60px;">
    <div class="row">
        <div class="col-md-8 col-md-offset-2 columns">
            <p class="text-center">
                <i class="fa fa-check-circle" aria-hidden="true" style="color:yellowgreen;font-size:50px;"></i>
            </p>
            <h2 class="text-center" style="font-size:60px;">Success!</h2>
            <hr style="border:2px solid yellowgreen;width:50px;"/><br/>
            <p class="text-center">
            Your submission has been successfully sent. <br/>We will contact you shortly. Thanks!
            </p>
        </div>
    </div>
</section>

<?php

get_footer();

?>