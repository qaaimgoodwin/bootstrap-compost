<!doctype html>

  <html class="no-js"  <?php language_attributes(); ?>>

	<head>
		<meta charset="utf-8">
		
		<!-- Force IE to use the latest rendering engine available -->
		<meta http-equiv="X-UA-Compatible" content="IE=edge">

		<!-- Mobile Meta -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta class="foundation-mq">
		
		<!-- If Site Icon isn't set in customizer -->
		<?php if ( ! function_exists( 'has_site_icon' ) || ! has_site_icon() ) { ?>
			<!-- Icons & Favicons -->
			<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon.png">
			<link href="<?php echo get_template_directory_uri(); ?>/assets/images/apple-icon-touch.png" rel="apple-touch-icon" />
			<!--[if IE]>
				<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
			<![endif]-->
			<meta name="msapplication-TileColor" content="#f01d4f">
			<meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/assets/images/win8-tile-icon.png">
	    	<meta name="theme-color" content="#121212">
	    <?php } ?>

		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
    <link href="https://fonts.googleapis.com/css?family=Amatic+SC:700|Roboto:400,400i,700,700i" rel="stylesheet">
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-T8Gy5hrqNKT+hzMclPo118YTQO6cYprQmhrYwIiQ/3axmI1hQomh7Ud2hPOy8SP1" crossorigin="anonymous">

		<?php wp_head(); ?>

		<!-- Drop Google Analytics here -->
		<!-- end analytics -->
		<!-- Slick slider -->
		  <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css"/>
<!-- 		  <script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js"></script>
 -->

			<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/vendor/hero-slider/css/style.css"/>
	</head>
	
	<!-- Uncomment this line if using the Off-Canvas Menu --> 
		
	<body <?php body_class(); ?>>
		<section id="green_bar">
      <div class="show-for-small-only expanded row">
      <div class="small-full medium-4 columns">
          <p class="topbar-p" class="text-center">1,564,017 lbs composted</p>
        </div>
      </div>
      <div class="expanded row">
        <div class="small-5 medium-4 columns">
          <p class="topbar-p topbar-p-lower" class="text-center">
          	Call Us (617)642-1979
          </p>
        </div>
        <div class="hide-for-small-only small-4 medium-4 columns">
          <p class="topbar-p" class="text-center">1,564,017 lbs composted</p>
        </div>
        <div class="small-7 medium-4 columns">
          <ul id="social_top">
		  	<li>
				<p class="topbar-p topbar-p-lower" class="text-center">Follow Us:&nbsp;<a href="https://twitter.com/compostboston" target="_blank"><i class="fa fa-twitter social-color" aria-hidden="true0"></i></a>&nbsp;<a href="https://www.instagram.com/bootstrapcompost/" target="_blank"><i class="fa fa-instagram social-color" aria-hidden="true0"></i></a>&nbsp;<a href="https://www.youtube.com/user/bootstrapcompost" target="_blank"><i class="fa fa-youtube social-color" aria-hidden="true0"></i></a>&nbsp;<a href="https://www.facebook.com/bootstrapcompost/" target="_blank"><i class="fa fa-facebook-official social-color" aria-hidden="true0"></i></a></p>  
			</li>
		  </ul>
        </div>
      </div>
    </section>

		<div class="off-canvas-wrapper">
			
			<div class="off-canvas-wrapper-inner" data-off-canvas-wrapper>
				
			<div class="off-canvas position-left" id="off-canvas" data-off-canvas data-position="left">
				<?php joints_off_canvas_nav(); ?>
			</div>
				
				<div class="off-canvas-content" data-off-canvas-content>
					
					<header class="header" role="banner">
							

						 <div class="" ><?php get_template_part( 'parts/nav', 'title-bar' ); ?></div>
		 	
					</header> 