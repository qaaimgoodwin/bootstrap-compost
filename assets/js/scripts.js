jQuery(document).foundation();
/*
These functions make sure WordPress
and Foundation play nice together.
*/

jQuery(document).ready(function() {

    // Remove empty P tags created by WP inside of Accordion and Orbit
    jQuery('.accordion p:empty, .orbit p:empty').remove();

    // Makes sure last grid item floats left
    jQuery('.archive-grid .columns').last().addClass('end');

    // Adds Flex Video to YouTube and Vimeo Embeds
    jQuery('iframe[src*="youtube.com"], iframe[src*="vimeo.com"]').each(function() {
        if (jQuery(this).innerWidth() / jQuery(this).innerHeight() > 1.5) {
            jQuery(this).wrap("<div class='widescreen flex-video'/>");
        } else {
            jQuery(this).wrap("<div class='flex-video'/>");
        }
    });

});

var elem = new Foundation.OffCanvas(jQuery('#off-canvas'));

jQuery(document).ready(function() {

    jQuery('.howItWorksSlider').slick({
        infinite: false,
        slidesToShow: 4,
        prevArrow: '<button type="button" class="slick-prev howItWorks-arrows arrow-left"><</button>',
        nextArrow: '<button type="button" class="slick-next howItWorks-arrows arrow-right">></button>',
        responsive: [{
            breakpoint: 1024,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 3,
            }
        }, {
            breakpoint: 600,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2
            }
        }, {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }]
    });

    jQuery('.pressSlider').slick({
        infinite: true,
        slidesToShow: 5,
        prevArrow: '<button type="button" class="slick-prev howItWorks-arrows arrow-left"><</button>',
        nextArrow: '<button type="button" class="slick-next howItWorks-arrows arrow-right">></button>',
        responsive: [{
            breakpoint: 1024,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 3,
                infinite: true
            }
        }, {
            breakpoint: 600,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2
            }
        }, {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }]
    });

    jQuery('.meetTheTeamSlider').slick({
        infinite: true,
        slidesToShow: 4,
        prevArrow: '<button type="button" class="slick-prev meet-the-team-arrows meet-the-team-arrow-left"><</button>',
        nextArrow: '<button type="button" class="slick-next meet-the-team-arrows meet-the-team-arrow-right">></button>',
        responsive: [{
            breakpoint: 1024,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 3,
                infinite: true
            }
        }, {
            breakpoint: 600,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2
            }
        }, {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }]
    });

    jQuery('.gearSlider').slick({
        infinite: true,
        slidesToShow: 4,
        prevArrow: '<button type="button" class="slick-prev meet-the-team-arrows meet-the-team-arrow-left"><</button>',
        nextArrow: '<button type="button" class="slick-next meet-the-team-arrows meet-the-team-arrow-right">></button>',
        responsive: [{
            breakpoint: 1024,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 3,
                infinite: true
            }
        }, {
            breakpoint: 600,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2
            }
        }, {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }]
    });



});
