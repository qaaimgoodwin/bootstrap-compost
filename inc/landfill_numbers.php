<!-- Landfill Numbers -->
<section id="landfill_numbers">
    <div class="row">
        <div class="medium-8 medium-offset-2 columns">
            <img src="<?php bloginfo('template_url')?>/assets/images/ticker_bucket.png" class="img-responsive float-center hide-for-medium hide-for-large show-for-small" alt="Bootstrap Compost">
            <h1 class="text-center landfill"><img src="<?php bloginfo('template_url')?>/assets/images/ticker_bucket.png" class="img-responsive hide-for-small-only" alt="Bootstrap Compost" style="padding-right:10px;">&nbsp;EXAMPLE NUMBER LBS OF ORGANICS SAVED FROM LANDFILLS</h1>
        </div>
    </div>
</section>