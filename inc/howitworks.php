<section style="padding-top:40px;padding-bottom:30px;">
    <div class="row">
        <div class="medium-12 columns">
            <div class="row" style="padding-top:30px;padding-bottom:30px">
                <div class="medium-8 medium-offset-2 columns">
                    <h2 class="text-center">SO HOW DOES IT WORK?</h2>
                    <h4 class="text-center">(FOR HOUSEHOLDS)</h4>
                </div>
            </div>
            <div class="row howitworks">
                <div class="medium-3 columns green_right_border">
                    <img src="<?php bloginfo('template_url')?>/assets/images/how1.jpg" class="float-center">
                    <h4 class="text-center number-amatic">1.</h4>
                    <p class="text-center">Fill out our sign-up form</p>
                </div>
                <div class="medium-3 columns green_right_border">
                    <img src="<?php bloginfo('template_url')?>/assets/images/how2.jpg" class="float-center">
                    <h4 class="text-center number-amatic">2.</h4>
                    <p class="text-center">We deliver a 5 gallon bucket to your door</p>
                </div>
                <div class="medium-3 columns green_right_border">
                    <img src="<?php bloginfo('template_url')?>/assets/images/how3.jpg" class="float-center">
                    <h4 class="text-center number-amatic">3.</h4>
                    <p class="text-center">You will fill it scraps, we pick it up &amp; replace it</p>
                </div>
                <div class="medium-3 columns">
                    <img src="<?php bloginfo('template_url')?>/assets/images/how4.jpg" class="float-center">
                    <h4 class="text-center number-amatic">4.</h4>
                    <p class="text-center">We compost for you &amp; you can get soil back</p>
                </div>
            </div>
        </div>
    </div>
</section>