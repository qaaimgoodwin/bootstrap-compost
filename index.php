<?php get_header(); ?>
<section>
		<div class="expanded row">
			<div class="medium-12 columns">
				<div class="featured-image-header">
					<h1 class="text-center">BLOG</h1>
					<img src="<?php bloginfo('template_url') ?>/assets/images/blog-img.jpg">
				</div>
			</div>
		</div>
	</section>
			
			<br/>
	<div id="content">
	
		<div id="inner-content" class="row">
	
		    <main id="main" class="large-8 medium-8 columns" role="main">
		    
			    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			 
					<!-- To see additional archive styles, visit the /parts directory -->
					<?php get_template_part( 'parts/loop', 'archive' ); ?>
				    
				<?php endwhile; ?>	

					
					
				<?php else : ?>
											
					<?php get_template_part( 'parts/content', 'missing' ); ?>
						
				<?php endif; ?>
																								
		    </main> <!-- end #main -->
		    
		    <?php get_sidebar(); ?>

		</div> <!-- end #inner-content -->

	</div> <!-- end #content -->

	<section>
				<div class="row">
					<div class="medium-8 medium-offset-2 columns">
						<?php joints_page_navi(); ?>
					</div>
				</div>
			</section>

<?php get_footer(); ?>