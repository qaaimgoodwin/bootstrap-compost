					<!-- cta -->
					<?php 
					include_once( '/inc/landfill_numbers.php');
					include_once('/inc/call_to_action.php');

					?>
					<footer class="footer" role="contentinfo">
						<div id="inner-footer" class="row">
					<div class="wrap">
							<!--<div class="large-12 medium-12 columns">
								<nav role="navigation">
		    						<?php joints_footer_links(); ?>
		    					</nav>
		    				</div> -->
							<!-- nav, bucket, bootstrap info -->
							<section id="above_copyright">
								<div class="row">
									<div class="medium-4 columns">
									<ul>
									<li>
										<h4>Quick Links</h4>
											<nav role="navigation">
												<?php joints_footer_links(); ?>
											</nav>
										</li>
									</ul>
									</div>
									<div class="medium-4 columns">
										<ul>
											<li>
												<h4>Who We Are</h4>
												<p>
													Bootstrap Compost is a residential and commercial food scrap pickup service operating in the Greater Boston area. 
													For a list of communities we service, <a href="#">click here</a>.
												</p>
											</li>
											<li>
												<h4>Contact Us</h4>
												<p>
												<a href="tel:+6176421979">(617) 642-1979</a><br/>
												<a href="mailto:bootstrapcompost@gmail.com">bootstrapcompost@gmail.com</a>
												</p>
											</li>
										</ul>
									</div>
									<div class="medium-4 columns">
										<img src="<?php bloginfo('template_url')?>/assets/images/bootstrap_bucket.png" alt="Bootstrap Compost" class="hide-for-small-only float-center">
									</div>
								</div>
							</section>
							<!-- footer line -->
							<div class="medium-12 columns" style="padding-bottom:30px;">
							<img src="<?php bloginfo('template_url')?>/assets/images/footer-line-lrg.png" class="float-center" alt="Footer Line">
							</div>
							<!-- copyright information -->
							<div class="large-12 medium-12 columns">
								<p class="source-org copyright copyright-text text-center"><img src="<?php bloginfo('template_url')?>/assets/images/bootstrap_footer.png" alt="Bootstrap Compost">&nbsp;&copy; <?php echo date('Y'); ?> <?php bloginfo('name'); ?>, INC. ALL RIGHTS RESERVED.</p>
							</div>
							</div>
						</div> <!-- end #inner-footer -->
					</footer> <!-- end .footer -->
				</div>  <!-- end .main-content -->
			</div> <!-- end .off-canvas-wrapper-inner -->
		</div> <!-- end .off-canvas-wrapper -->
		<?php wp_footer(); ?>
	</body>
	<script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/vendor/hero-slider/js/main.js"></script>
	
</html> <!-- end page -->