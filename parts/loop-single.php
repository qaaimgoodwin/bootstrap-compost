<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
						
	<header class="article-header">	
		<!--<div class="text-center"><?php the_post_thumbnail('full'); ?></div>-->
    </header> <!-- end article header --><br/>
					
    <section class="entry-content" itemprop="articleBody">
		<h1 class="entry-title single-title text-center" itemprop="headline" style="font-size:55px;"><?php the_title(); ?></h1>
		<hr/>
		<?php get_template_part( 'parts/content', 'byline-single' ); ?>
		<?php the_content(); ?>
	</section> <!-- end article section -->
						
	<footer class="article-footer">
		<?php wp_link_pages( array( 'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'jointswp' ), 'after'  => '</div>' ) ); ?>
		<p class="tags"><?php the_tags('<span class="tags-title">' . __( 'Tags:', 'jointswp' ) . '</span> ', ', ', ''); ?></p>	
	</footer> <!-- end article footer -->
						
	<?php comments_template(); ?>	
													
</article> <!-- end article -->