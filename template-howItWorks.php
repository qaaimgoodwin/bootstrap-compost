<?php
/*
Template Name: How It Works
*/
?>

<?php get_header(); ?>
	<section>
		<div class="expanded row">
			<div class="medium-12 columns">
				<div class="featured-image-header">
					<h1 class="text-center hero-h1-bigger"><?php the_title(); ?></h1>
					<?php if ( has_post_thumbnail()) : the_post_thumbnail( 'full' ); endif; ?>
				</div>
			</div>
		</div>
	</section>
	<section class="page-content">
		<div class="wrap">
			<div id="content" class="content_padding">
			
				<div id="inner-content" class="row">
			
				    <main id="main" class="medium-8 medium-offset-2 columns" role="main">
						
						<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

							<?php get_template_part( 'parts/loop', 'page' ); ?>
							
						<?php endwhile; endif; ?>							

					</main> <!-- end #main -->
				    
				</div> <!-- end #inner-content -->
			
			</div> <!-- end #content -->
		</div>
	</section>
	<!-- How it works -->
	<section class="howItWorks">
	    <div class="row">
	        <div class="small-full columns">
	            <h2>SO HOW DOES IT WORK?</h2>
	            <h3>(FOR HOUSEHOLDS)</h3>
	        </div>
	    </div>
	    <div class="wrap">
	        <div class="howItWorksSlider">
	          <div>
	          <img class="float-center" src="<?php echo get_template_directory_uri(); ?>/assets/images/how1.jpg" alt="">
	          <div class="row">
	              <div class="small-full columns">
	                  <h2 class="howItWorks-h2">1.</h2>
	                  <p class="howItWorks-p">Fill out sign-up form</p>
	              </div>
	          </div>
	          </div>
	          <div class="divider-after">
	          <img class="float-center" src="<?php echo get_template_directory_uri(); ?>/assets/images/how2.jpg" alt="">
	          <div class="row">
	              <div class="small-full columns">
	                  <h2 class="howItWorks-h2">2.</h2>
	                  <p class="howItWorks-p">We deliver a 5 gallon bucket<br>to your door</p>
	              </div>
	          </div>
	          </div>
	          <div class="divider-after">
	          <img class="float-center" src="<?php echo get_template_directory_uri(); ?>/assets/images/how3.jpg" alt="">
	          <div class="row">
	              <div class="small-full columns">
	                  <h2 class="howItWorks-h2">3.</h2>
	                  <p class="howItWorks-p">You fill it up with scraps,<br>we'll pick it up &amp; replace it</p>
	              </div>
	          </div>
	          </div>
	          <div class="divider-after">
	          <img class="float-center" src="<?php echo get_template_directory_uri(); ?>/assets/images/how4.jpg" alt="">
	          <div class="row">
	              <div class="small-full columns">
	                  <h2 class="howItWorks-h2">4.</h2>
	                  <p class="howItWorks-p">We compost for you &amp;<br>you get soil back</p>
	              </div>
	          </div>
	          </div>
	        </div>
	    </div>
	</section>
	<section class="email-cap">
	<div class="row wrap">
	<div class="medium-4 large-4 columns">
	    <h2 class="email-h2">Stay Connected</h2>
	</div>
	    <div class="medium-8 large-8 columns">
	        <div class="row">
	            <div class="medium-full large-full columns">
	                <h3 class="email-h3">Sign up for our newsletter and stay informed about events, how-tos, offers, and more.</h3>
	            </div>
	            <div class="medium-full large-full columns">
	                <div class="input-group">
	                  <input placeholder="Enter your email address" class="input-group-field" type="text">
	                  <div class="input-group-button">
	                    <input type="submit" class="button" value="Submit">
	                  </div>
	                </div>            
	            </div>
	        </div>
	    </div>
	</div>    
	</section>
<?php get_footer(); ?>