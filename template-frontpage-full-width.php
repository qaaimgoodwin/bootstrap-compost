<?php
/*
Template Name: Frontpage Full Width (No Sidebar)
*/
?>

<?php get_header(); ?>
	<!-- hero image -->
    <section class="cd-hero">
   
        <ul class="cd-hero-slider autoplay">
            <li class="selected ">
                <div class="cd-full-width">
                    <div class="hero-video-overlay"></div>
                    <h1 class="hero-h1">WHY BOOTSTRAP?</h1>
<!--                     <h1 class="hero-h2">SPELL IT OUT WITH BOOTSTRAP</h1>
 -->                    <a href="https://bootstrapcompost.wordpress.com/2014/09/18/the-abcs-of-bsc/" target="_blank" class="button hero-button"><h3>LEARN MORE</h3></a>         
                    <!-- <img src="<?php echo get_template_directory_uri(); ?>/assets/images/slide-1.jpg" alt="tech 1"> -->
                </div> <!-- .cd-full-width -->
            </li>
            <li class=" ">
                <div class="cd-full-width">
                    <div class="hero-video-overlay"></div>
                    <h1 class="hero-h1 ">AT HOME &amp; AT WORK!</h1>
<!--                     <h1 class="hero-h2">WE GOT YOUR OFFICE COVERED</h1>
 -->                    <a href="<?php echo get_permalink( get_page_by_path( 'office-service' ) ) ?>" target="" class="button hero-button"><h3>LEARN MORE</h3></a>         
                    <!-- <img src="<?php echo get_template_directory_uri(); ?>/assets/images/slide-1.jpg" alt="tech 1"> -->
                </div> <!-- .cd-full-width -->
            </li>
            <li class="cd-bg-video">
                <h1 class="hero-h1 ">HEAR FROM OUR CLIENTS</h1>
<!--                 <h1 class="hero-h2">WE GOT YOUR OFFICE COVERED</h1>
 -->                <a href="https://youtu.be/creo72bkjK0" target="_blank" class="button hero-button"><h3>LEARN MORE</h3></a>                 
                <div class="cd-full-width">
                </div> <!-- .cd-full-width -->

                <div class="cd-bg-video-wrapper" data-video="<?php echo get_template_directory_uri(); ?>/assets/images/video/hero-video">
                <div class="hero-video-overlay"></div>
                    <!-- video element will be loaded using jQuery -->
                </div> <!-- .cd-bg-video-wrapper -->
            </li>            
            <li class=" ">
                <div class="cd-full-width">
                    <div class="hero-video-overlay"></div>
                    <h1 class="hero-h1 ">THE BIG STINK:<br>A BLOG SERIES</h1>
<!--                     <h1 class="hero-h2">WE GOT YOUR OFFICE COVERED</h1>
 -->                    <a href="https://bootstrapcompost.wordpress.com/category/the-big-stink/" target="_blank" class="button hero-button"><h3>LEARN MORE</h3></a>         
                    <!-- <img src="<?php echo get_template_directory_uri(); ?>/assets/images/slide-1.jpg" alt="tech 1"> -->
                </div> <!-- .cd-full-width -->
            </li>



        </ul> <!-- .cd-hero-slider -->

        <!-- <div class="cd-slider-nav">
            <nav>
                <span class="cd-marker item-1"></span>
                
                <ul>
                    <li class="selected"><a href="#0"></a></li>
                    <li><a href="#0"></a></li>
                    <li><a href="#0"></a></li>
                    <li><a href="#0"></a></li>
                </ul>
            </nav> 
        </div> --> <!-- .cd-slider-nav -->

    </section> <!-- .cd-hero -->

    <section class="are-you-a show-for-large">
        <div class="row">
            <div class="small-full medium-4 columns">
                <a href="<?php echo get_permalink( get_page_by_path( 'home' ) ) ?>" class="button are-you-a-button float-center">
                    <h2 class="are-you-a-h2">ARE YOU A HOUSEHOLD?</h2>
                </a>
            </div>
            <div class="small-full medium-4 columns">
                <a href="<?php echo get_permalink( get_page_by_path( 'office-service' ) ) ?>" class="button are-you-a-button float-center">
                    <h2 class="are-you-a-h2">ARE YOU AN OFFICE?</h2>
                </a>
            </div>
            <div class="small-full medium-4 columns">
                <a href="<?php echo get_permalink( get_page_by_path( 'restaurant' ) ) ?>" class="button are-you-a-button float-center">
                    <h2 class="are-you-a-h2">ARE YOU A RESTAURANT/CAFE?</h2>
                </a>
            </div>
        </div>
    </section>
    <section class="are-you-a show-for-small hide-for-large">
        <a class="button float-center are-you-a-dropdown-button" type="button" data-toggle="are-you-a-dropdown"><h2 class="are-you-a-h2">ARE YOU A</h2></a>
        <div class="dropdown-pane" id="are-you-a-dropdown" data-dropdown data-auto-focus="true">
            <div class="row">
                <div class="small-full medium-4 columns">
                    <a href="<?php echo get_permalink( get_page_by_path( 'home' ) ) ?>" class="button are-you-a-button float-center">
                        <h2 class="are-you-a-h2">HOUSEHOLD?</h2>
                    </a>
                </div>
                <div class="small-full medium-4 columns">
                    <a href="<?php echo get_permalink( get_page_by_path( 'office-service' ) ) ?>" class="button are-you-a-button float-center">
                        <h2 class="are-you-a-h2">OFFICE?</h2>
                    </a>
                </div>
                <div class="small-full medium-4 columns">
                    <a href="<?php echo get_permalink( get_page_by_path( 'restaurant' ) ) ?>" class="button are-you-a-button float-center">
                        <h2 class="are-you-a-h2">RESTAURANT/CAFE?</h2>
                    </a>
                </div>
            </div>
        </div>  
    </section>
  




<!-- How it works -->
<section class="howItWorks">
    <div class="row">
        <div class="small-full columns">
            <h2>SO HOW DOES IT WORK?</h2>
            <h3>(FOR HOUSEHOLDS)</h3>
        </div>
    </div>
    <div class="wrap">
        <div class="howItWorksSlider">
          <div>
          <img class="float-center" src="<?php echo get_template_directory_uri(); ?>/assets/images/how1.jpg" alt="">
          <div class="row">
              <div class="small-full columns">
                  <h2 class="howItWorks-h2">1.</h2>
                  <p class="howItWorks-p">Fill out <a href="<?php echo get_permalink( get_page_by_path( 'sign-up' ) ) ?>">sign-up</a> form</p>
              </div>
          </div>
          </div>
          <div class="divider-after">
          <img class="float-center" src="<?php echo get_template_directory_uri(); ?>/assets/images/how2.jpg" alt="">
          <div class="row">
              <div class="small-full columns">
                  <h2 class="howItWorks-h2">2.</h2>
                  <p class="howItWorks-p">We deliver a 5 gallon bucket<br>to your door</p>
              </div>
          </div>
          </div>
          <div class="divider-after">
          <img class="float-center" src="<?php echo get_template_directory_uri(); ?>/assets/images/how3.jpg" alt="">
          <div class="row">
              <div class="small-full columns">
                  <h2 class="howItWorks-h2">3.</h2>
                  <p class="howItWorks-p">You fill it up with scraps,<br>we'll pick it up &amp; replace it</p>
              </div>
          </div>
          </div>
          <div class="divider-after">
          <img class="float-center" src="<?php echo get_template_directory_uri(); ?>/assets/images/how4.jpg" alt="">
          <div class="row">
              <div class="small-full columns">
                  <h2 class="howItWorks-h2">4.</h2>
                  <p class="howItWorks-p">We compost for you &amp;<br>you get soil back</p>
              </div>
          </div>
          </div>
        </div>
    </div>
</section>



    <?php 
    
    include_once('/inc/howitworks_frontmenu.php');
    include_once('/inc/howitworks.php');
    
    ?>
	<div id="content">
	
		<div id="inner-content" class="row slider-row">
	
		    <main id="main" class="large-12 medium-12 columns" role="main">
				
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					<?php get_template_part( 'parts/loop', 'frontpage' ); ?>
					
				<?php endwhile; endif; ?>							

			</main> <!-- end #main -->
		    
		</div> <!-- end #inner-content -->
	
	</div> <!-- end #content -->
<!-- Highlights -->
<div class="rough-edges">
<div class="rough-edge-top"></div>
    <section id="highlights">
        <div class="row" data-equalizer="foo">
          <div class="medium-4 columns">
            <div class="callout panel panel-no-bg" data-equalizer-watch="foo">
            <div id="callout-1" class="callout panel callout-base" data-equalizer-watch="bar">
                <!-- <img src="<?php echo get_template_directory_uri(); ?>/assets/images/callout-1.jpg" alt=""> -->
            </div>
            <div class="callout panel" data-equalizer-watch="bar">
                <h2 class="hl-h2">EDUCATION</h2>
                <p class="hl-p">We donate a portion of our compost to school and community gardening projects. We strive to raise awareness about food waste, composting, food systems and, ultimately, nutrition, through workshops, tours and presentations.</p>
                <a href="<?php echo get_permalink( get_page_by_path( 'consulting' ) ) ?>" class="button hl-button float-center">READ MORE</a>
            </div>
            </div>
          </div>
          <div class="medium-4 columns">
            <div class="callout panel panel-no-bg" data-equalizer-watch="foo">
            <div id="callout-2" class="callout panel callout-base" data-equalizer-watch="bar">
                <!-- <img src="<?php echo get_template_directory_uri(); ?>/assets/images/callout-1.jpg" alt=""> -->
            </div>
            <div class="callout panel" data-equalizer-watch="bar">
                <h2 class="hl-h2">IMPACT</h2>
                <p class="hl-p">Through composting, we are keeping food waste out of landfills. By doing so, we are offsetting harmful GHGs, creating soil amendment for farms, gardens, schools and households throughout Greater Boston. See our stats here.</p>
                <a href="<?php echo get_permalink( get_page_by_path( 'impact' ) ) ?>" class="button hl-button float-center">READ MORE</a>
            </div>
            </div>
          </div>
          <div class="medium-4 columns">
            <div class="callout panel panel-no-bg" data-equalizer-watch="foo">
            <div id="callout-3" class="callout panel callout-base" data-equalizer-watch="bar">
                <!-- <img src="<?php echo get_template_directory_uri(); ?>/assets/images/callout-1.jpg" alt=""> -->
            </div>
            <div class="callout panel" data-equalizer-watch="bar">
                <h2 class="hl-h2">TESTIMONIAL</h2>
                <p class="hl-p">"I have two children. They are helpful in dumping their leftovers into the bin.I have also told lots of friends about Bootstrap. They always think it’s a great idea. It's a great service, very convenient."<br><strong>Zachary, Jamaica Plain</strong></p>
                <a href="<?php echo get_permalink( get_page_by_path( 'testimonials' ) ) ?>" class="button hl-button float-center">VIEW MORE</a>
            </div>
            </div>
          </div>  
          </div>        
    <div class="rough-edge-bottom"></div>
    </section>
</div>
<!-- Press -->
<section class="press">
    <div class="pressSlider">
      <div>
        <img class="float-center" src="<?php echo get_template_directory_uri(); ?>/assets/images/press/press-1.jpg" alt="">
        
      </div>
      <div>
        <img class="float-center" src="<?php echo get_template_directory_uri(); ?>/assets/images/press/press-2.jpg" alt="">
        
      </div>
      <div>
        <img class="float-center" src="<?php echo get_template_directory_uri(); ?>/assets/images/press/press-3.jpg" alt="">
        
      </div>
      <div>
        <img class="float-center" src="<?php echo get_template_directory_uri(); ?>/assets/images/press/press-4.jpg" alt="">
        
      </div>
      <div>
        <img class="float-center" src="<?php echo get_template_directory_uri(); ?>/assets/images/press/press-5.jpg" alt="">
        
      </div>
      <div>
        <img class="float-center" src="<?php echo get_template_directory_uri(); ?>/assets/images/press/press-6.jpg" alt="">
        
      </div>
      <div>
        <img class="float-center" src="<?php echo get_template_directory_uri(); ?>/assets/images/press/press-7.jpg" alt="">
        
      </div>
      <div>
        <img class="float-center" src="<?php echo get_template_directory_uri(); ?>/assets/images/press/press-8.jpg" alt="">
        
      </div>
      <div>
        <img class="float-center" src="<?php echo get_template_directory_uri(); ?>/assets/images/press/press-9.jpg" alt="">
        
      </div>
      <div>
        <img class="float-center" src="<?php echo get_template_directory_uri(); ?>/assets/images/press/press-10.jpg" alt="">
        
      </div>
    </div>

</section>


<section class="email-cap">
<div class="row wrap">
<div class="medium-4 large-4 columns">
    <h2 class="email-h2">Stay Connected</h2>
</div>
    <div class="medium-8 large-8 columns">
        <div class="row">
            <div class="medium-full large-full columns">
                <h3 class="email-h3">Sign up for our newsletter and stay informed about events, how-tos, offers, and more.</h3>
            </div>
            <div class="medium-full large-full columns">
                <div class="input-group">
                  <input placeholder="Enter your email address" class="input-group-field" type="text">
                  <div class="input-group-button">
                    <input type="submit" class="button" value="Submit">
                  </div>
                </div>            
            </div>
        </div>
    </div>
</div>    
</section>

<?php get_footer(); ?>
