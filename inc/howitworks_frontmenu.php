<section>
    <div class="expanded row">
        <div class="medium-12 columns">
            <div class="row">
                <div class="medium-8 medium-offset-2 columns">
                    <ul id="frontmenu">
                        <li class="front-menu-right-padding"><a href="#"class="text-center">ARE YOU A HOUSEHOLD?</a></li>
                        <li class="front-menu-right-padding"><a href="#" class="text-center">ARE YOU AN OFFICE?</a></li>
                        <li><a href="#" class="text-center">ARE YOU A RESTAURANT/CAFE?</a></li>
                    </ul>
                    <img src="<?php bloginfo('template_url')?>/assets/images/footer-line.png" class="float-center">
                </div>
            </div>
        </div>
    </div>
</section>