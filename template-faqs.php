<?php
/*
Template Name: FAQs Full Width (No Sidebar)
*/
?>

<?php get_header(); ?>
	<section>
		<div class="expanded row">
			<div class="medium-12 columns">
				<div class="featured-image-header">
					<h1 class="text-center"><?php the_title(); ?></h1>
					<?php if ( has_post_thumbnail()) : the_post_thumbnail( 'full' ); endif; ?>
				</div>
			</div>
		</div>
	</section>		
	<div id="content" class="content_padding">
	
		<div id="inner-content" class="row">
	
		    <main id="main" class="medium-8 medium-offset-2 columns" role="main">
				
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					<?php get_template_part( 'parts/loop', 'page' ); ?>
					
				<?php endwhile; endif; ?>	
                <!-- questions -->
                 <?php 
                $i = 1;
                $qa = 1;
                ?>
                <section style="padding-top:30px;padding-bottom:40px;">
                <?php if(get_field('faq')): ?>
                <?php while(has_sub_field('faq')): ?>
                  <div>
                    <h3>
                      <a href="#question-#-<?php echo $i++?>">&bull;&nbsp;<?php the_sub_field('question') ?></a>
                    </h3>
                  </div>
                  <?php endwhile; ?>
                    <?php endif; ?>
                </section>
                <!-- answers -->
                <section>
                <?php if(get_field('faq')): ?>
                <?php while(has_sub_field('faq')): ?>
                  <div id="question-#-<?php echo $qa++ ?>" style="background-color:whitesmoke;padding:30px;border-radius:3px;">
                    <p>
                      <strong>Question:&nbsp;<?php the_sub_field('question') ?></strong>
                    </p>
                    <hr style="border:1px solid yellowgreen"/>
                    <p style="font-size:12px;">
                    <em><?php the_sub_field('answer') ?></em>
                    </p>
                    <p style="font-size:11px;">
                    <a href="#"><i class="fa fa-angle-up" aria-hidden="true"></i>&nbsp;Back To Top</a>
                    </p>
                  </div><br/>
                   <?php endwhile; ?>
                    <?php endif; ?>
                </section>

			</main> <!-- end #main -->
		    
		</div> <!-- end #inner-content -->
	
	</div> <!-- end #content -->

<?php get_footer(); ?>