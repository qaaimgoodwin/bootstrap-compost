<?php
/*
Template Name: Blog Posts Full Width (No Sidebar)
*/
?>
<?php get_header(); ?>
<section style="padding-top:30px;padding-bottom:20px;">
<div class="expanded row">
    <div class="medium-8 columns">
        <p class="text-center">
            nothing here.
        </p>
        <div>
        <?php the_posts_pagination( array( 
                'mid_size' => 2,
                'prev_text' => __( 'Back', 'textdomain'),
                'next_text' => __( 'Onward', 'textdomain'),
        ) ); ?>
        </div>
    </div>
    <div class="medium-4 columns">
    <?php get_sidebar(); ?>
    </div>
</div>
</section>
<?php 
get_footer(); ?>